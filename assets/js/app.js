// We import the CSS which is extracted to its own file by esbuild.
// Remove this line if you add a your own CSS build pipeline (e.g postcss).
// import "../css/app.css"

// If you want to use Phoenix channels, run `mix help phx.gen.channel`
// to get started and then uncomment the line below.
// import "./user_socket.js"

// You can include dependencies in two ways.
//
// The simplest option is to put them in assets/vendor and
// import them using relative paths:
//
//     import "../vendor/some-package.js"
//
// Alternatively, you can `npm install some-package --prefix assets` and import
// them using a path starting with the package name:
//
//     import "some-package"
//

// Include phoenix_html to handle method=PUT/DELETE in forms and buttons.
import "phoenix_html"
// Establish Phoenix Socket and LiveView configuration.
import {Socket} from "phoenix"
import {LiveSocket} from "phoenix_live_view"
import topbar from "../vendor/topbar"

let csrfToken = document.querySelector("meta[name='csrf-token']").getAttribute("content")
// BEGIN BLIXXIR INITIAL SETUP ROBOTIC INSERT
// import Alpine
import Alpine from "alpinejs";

window.Alpine = Alpine;
Alpine.start();


var timer_running = false;


function process_timer() {
  max     = document.getElementById('max').value;
  seconds = document.getElementById('secs').value;
  if (seconds!=-1) {
    seconds = seconds - 1;
    if (seconds<0) {
      seconds = 0;
    }
    document.getElementById('secs').value = seconds;
    var bar = document.getElementById('bar');
    if (seconds==0) {
      bar.innerHTML= "";
    } else {
      bar.innerHTML= seconds + " secs";
    }
    percent = Math.trunc((seconds * 100.0) / max);
    bar.style.width = percent + "%";
    if (seconds>0) {
      setTimeout(process_timer, 1000);
    } else {
      timer_running = false;
      play_sound();
      var iter = document.getElementById('iterator').value;
      iter = iter + 1;
      document.getElementById('iterator').value = iter;
    }
  } else {
    timer_running = false;
    document.getElementById('secs').value = 0;
  }
}

function start_timer() {
  if (! timer_running) {
    timer_running = true;
    setTimeout(process_timer, 1000);
  }
}

let hooks = {};
hooks.startTimer = {
  updated() {
    /* alert("Updated hook"); */
    var mx = document.getElementById('max').value; 
    if (mx > 1) {
      start_timer();
    }
  }
}


hooks.buttonValue = {
  mounted() {
    this.el.addEventListener("click", this.clickListener.bind(this))
  },
  clickListener() {
    const hiddenInput = document.querySelector(`input[type='hidden'][name='${this.el.name}']`)
    if (hiddenInput) {
      hiddenInput.value = this.el.value
    } else {
      const input = document.createElement("input")
      input.setAttribute("type", "hidden")
      input.setAttribute("name", this.el.name)
      input.setAttribute("value", this.el.value)
      this.el.closest("form").append(input)
    }
  }
} 

/* export default buttonValue */

let liveSocket = new LiveSocket("/live", Socket, {
  params: { _csrf_token: csrfToken },
  hooks: hooks,
  dom: {
    onBeforeElUpdated(from, to) {
      /*if (from.id=="max") {
        if (to.value>1) {
          start_timer();
        }
      }*/
      if (from._x_dataStack) {
        window.Alpine.clone(from, to);
      }
    },
  },
});
//  END BLIXXIR INITIAL SETUP ROBOTIC INSERT

// Show progress bar on live navigation and form submits
topbar.config({barColors: {0: "#29d"}, shadowColor: "rgba(0, 0, 0, .3)"})
window.addEventListener("phx:page-loading-start", info => topbar.show())
window.addEventListener("phx:page-loading-stop", info => topbar.hide())

// connect if there are any LiveViews on the page
liveSocket.connect()

// expose liveSocket on window for web console debug logs and latency simulation:
// >> liveSocket.enableDebug()
// >> liveSocket.enableLatencySim(1000)  // enabled for duration of browser session
// >> liveSocket.disableLatencySim()
window.liveSocket = liveSocket


const audio = new Audio("sounds/magic.mp3");
function play_sound() {
  audio.play();
}