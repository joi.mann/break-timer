defmodule BreakTimer.Repo.Migrations.CreateTagdefs do
  use Ecto.Migration

  def change do
    create table(:tagdefs) do
      add :user_id,     :integer
      add :name,        :string
      add :description, :string
      add :tag_index,   :integer  # >2
      add :tag_offset,  :integer  # 0-63
      add :inactive,    :boolean, default: false, null: false
      add :deleted,     :boolean, default: false, null: false
      add :proj_id,     :integer

      timestamps([type: :utc_datetime_usec])
    end
  end
end
