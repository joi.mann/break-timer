defmodule BreakTimer.Repo.Migrations.CreatePunches do
  use Ecto.Migration

  def change do
    create table(:punches) do
      add :ptype,   :integer
      add :tags,    :bigint
      add :note,    :string
      add :deleted, :boolean, default: false, null: false
      add :proj_id, :integer

      timestamps([type: :utc_datetime_usec])
    end
  end
end
