defmodule BreakTimer.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :user_id, :integer
      add :pred_id, :integer
      add :full_name, :string
      add :first, :string
      add :middle, :string
      add :last, :string
      add :handle, :string
      #add :dtstamp, :utc_datetime_usec
      #add :udtstamp, :utc_datetime_usec
      add :inactive, :boolean, default: false, null: false
      add :deleted,  :boolean, default: false, null: false

      timestamps()
    end
  end
end
