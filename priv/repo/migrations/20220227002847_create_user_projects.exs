defmodule BreakTimer.Repo.Migrations.CreateUserProjects do
  use Ecto.Migration

  def change do
    create table(:user_projects) do
      add :user_id,  :integer
      add :proj_id,  :integer
      add :inactive, :boolean, default: false, null: false
      #add :dtstamp, :utc_datetime_usec
      #add :udtstamp, :utc_datetime_usec
      add :deleted,  :boolean, default: false, null: false

      timestamps([type: :utc_datetime_usec])
    end
  end
end
