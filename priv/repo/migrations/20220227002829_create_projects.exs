defmodule BreakTimer.Repo.Migrations.CreateProjects do
  use Ecto.Migration

  def change do
    create table(:projects) do
      
      add :name,     :string
      add :owner_id, :integer
      #add :dtstamp, :utc_datetime_usec
      #add :udtstamp, :utc_datetime_usec
      add :inactive, :boolean, default: false, null: false
      add :deleted,  :boolean, default: false, null: false

      timestamps([type: :utc_datetime_usec])
    end
  end
end
