mix phx.gen.schema Punch punches dtstamp:utc_datetime_usec ptype:integer tags:integer note:string  deleted:boolean  proj_id:integer udtstamp:utc_datetime_usec
# postgres type                                             :integer      :bigint
#                                                           + pos start   0-63 index into tag set 
#                                                           - neg stop
#                                                           1 = no tag data
#                                                           0 = not start/stop - bad click
#                                                           |index| into tag db

mix phx.gen.schema TagDef tagdefs user_id:integer name:string tag_index:integer  tag_offset:string  active:boolean proj_id:integer udtstamp:utc_datetime_usec
# postgres type                                                                                     :bit(6)
#                                                                                  >1

mix phx.gen.schema User users user_id:integer pred_id:integer full_name:string first:string middle:string last:string handle:string dtstamp:utc_datetime_usec udtstamp:utc_datetime_usec inactive:boolean deleted:boolean

mix phx.gen.schema Project projects name:string owner_id:integer dtstamp:utc_datetime_usec udtstamp:utc_datetime_usec inactive:boolean deleted:boolean

mix phx.gen.schema UserProject user_projects user_id:integer proj_id:integer enabled:boolean dtstamp:utc_datetime_usec udtstamp:utc_datetime_usec  deleted:boolean
