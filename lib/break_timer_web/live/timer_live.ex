defmodule BreakTimerWeb.TimerLive do
  require Logger
  use BreakTimerWeb, :live_view

  alias BreakTimer.Timer

  def taglist do
    [[key: "tagA", value: "tagA"],[key: "tagB", value: "tagB"],[key: "tagC", value: "tagC"],[key: "", value: ""]]
  end

  def mount(_,csrf_token, socket) do
    timer = %Timer{
                    bar:        "width:0%",
                    seconds:     0,
                    max:         1,
                    iterator:    0
                  }
    assigns = [
      conn:        socket,
      #action:      action,
      #csrf_token:  csrf_token,
      bar:        "width:0%",
      seconds:     "0",
      max:         "1",
      iterator:    "0",
      taglist:     taglist,
      changeset:   Timer.changeset(timer,%{
                                              bar:        "width:0%",
                                              seconds:     "0",
                                              max:         "1",
                                              iterator:    "0"
                                            })
    ]
    {:ok, assign(socket,assigns)}
  end

  def handle_event("start_timer", %{"_csrf_token" => csrf_token, "timer" => timer}, socket) do
    %{"max"      => max,
      "note"     => note,
      "seconds"  => secs,
      "time"     => minutes,
      "iterator" => iter,
      "tags"     => tags  } = timer
    Logger.debug("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!      time:#{minutes}", ansi_color: :yellow)    
    Logger.debug("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!      data:#{secs}", ansi_color: :yellow)    
    Logger.debug("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!start_timer", ansi_color: :yellow)    

    {sec, _ } = Integer.parse(secs)
    save_sec = sec
    {isecs, _} = Integer.parse(minutes)

    {sec, max} = if (isecs==0) do
      {-1,"1"}
    else
      if (sec>0) do
        {sec - 1, max}
      else
        {isecs, minutes}
      end
    end

    {mx, _ } = Integer.parse(max)
    bar      = "width:#{div((sec * 100),mx)}%"

    {it, _}  = Integer.parse(iter)
    iterator = if (save_sec == 0) do
      it + 1
    else
      it
    end
    assigns  = assign(socket, %{bar: bar, seconds: sec, max: mx, iterator: iterator, taglist: taglist})
    {:noreply, assigns}
  end

  def handle_event("on30", _, socket) do
    seconds = 30 * 60
    bar     = "width:100%"
    socket  = assign(socket, %{bar: bar, seconds: seconds, max: seconds, taglist: taglist})
    {:noreply, socket}
  end

end