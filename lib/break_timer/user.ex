defmodule BreakTimer.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :deleted, :boolean, default: false
    field :dtstamp, :utc_datetime_usec
    field :first, :string
    field :full_name, :string
    field :handle, :string
    field :inactive, :boolean, default: false
    field :last, :string
    field :middle, :string
    field :pred_id, :integer
    field :udtstamp, :utc_datetime_usec
    field :user_id, :integer

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:user_id, :pred_id, :full_name, :first, :middle, :last, :handle, :dtstamp, :udtstamp, :inactive, :deleted])
    |> validate_required([:user_id, :pred_id, :full_name, :first, :middle, :last, :handle, :dtstamp, :udtstamp, :inactive, :deleted])
  end
end
