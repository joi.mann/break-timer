defmodule BreakTimer.Timer do
	#defstruct [:mode,:time,:note,:tag,:bar,:max,:seconds]
	@types %{mode: :boolean, time: :timestamp, note: :string, tag: :string,bar: :integer,max: :integer,seconds: :integer,iterator: :integer}

	alias  BreakTimer.Timer
	use    Ecto.Schema
	import Ecto.Changeset

	schema "timers" do
		field :mode
		field :time
		field :max,      :integer
		field :bar,      :string
		field :seconds,  :integer
		field :note,     :string
		field :tag,      :string
		field :iterator, :integer
	end	

	def changeset(timer, attrs) do
		timer
		|> cast(attrs, Map.keys(@types))
		#|> validate_required([:game_name, :email])
		#|> validate_format(:email, ~r/@/)
	end	

end
