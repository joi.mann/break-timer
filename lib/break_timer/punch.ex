defmodule BreakTimer.Punch do
  use Ecto.Schema
  import Ecto.Changeset

  schema "punches" do
    field :deleted, :boolean, default: false
    field :dtstamp, :utc_datetime_usec
    field :note, :string
    field :proj_id, :integer
    field :ptype, :integer
    field :tags, :integer
    field :udtstamp, :utc_datetime_usec

    timestamps()
  end

  @doc false
  def changeset(punch, attrs) do
    punch
    |> cast(attrs, [:dtstamp, :ptype, :tags, :note, :deleted, :proj_id, :udtstamp])
    |> validate_required([:dtstamp, :ptype, :tags, :note, :deleted, :proj_id, :udtstamp])
  end
end
