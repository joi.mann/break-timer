defmodule BreakTimer.Repo do
  use Ecto.Repo,
    otp_app: :break_timer,
    adapter: Ecto.Adapters.Postgres
end
