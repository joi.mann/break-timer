defmodule BreakTimer.TagDef do
  use Ecto.Schema
  import Ecto.Changeset

  schema "tagdefs" do
    field :active, :boolean, default: false
    field :name, :string
    field :proj_id, :integer
    field :tag_index, :integer
    field :tag_offset, :string
    field :udtstamp, :utc_datetime_usec
    field :user_id, :integer

    timestamps()
  end

  @doc false
  def changeset(tag_def, attrs) do
    tag_def
    |> cast(attrs, [:user_id, :name, :tag_index, :tag_offset, :active, :proj_id, :udtstamp])
    |> validate_required([:user_id, :name, :tag_index, :tag_offset, :active, :proj_id, :udtstamp])
  end
end
