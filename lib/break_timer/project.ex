defmodule BreakTimer.Project do
  use Ecto.Schema
  import Ecto.Changeset

  schema "projects" do
    field :deleted, :boolean, default: false
    field :dtstamp, :utc_datetime_usec
    field :inactive, :boolean, default: false
    field :name, :string
    field :owner_id, :integer
    field :udtstamp, :utc_datetime_usec

    timestamps()
  end

  @doc false
  def changeset(project, attrs) do
    project
    |> cast(attrs, [:name, :owner_id, :dtstamp, :udtstamp, :inactive, :deleted])
    |> validate_required([:name, :owner_id, :dtstamp, :udtstamp, :inactive, :deleted])
  end
end
