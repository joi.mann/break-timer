defmodule BreakTimer.UserProject do
  use Ecto.Schema
  import Ecto.Changeset

  schema "user_projects" do
    field :deleted, :boolean, default: false
    field :dtstamp, :utc_datetime_usec
    field :enabled, :boolean, default: false
    field :proj_id, :integer
    field :udtstamp, :utc_datetime_usec
    field :user_id, :integer

    timestamps()
  end

  @doc false
  def changeset(user_project, attrs) do
    user_project
    |> cast(attrs, [:user_id, :proj_id, :enabled, :dtstamp, :udtstamp, :deleted])
    |> validate_required([:user_id, :proj_id, :enabled, :dtstamp, :udtstamp, :deleted])
  end
end
